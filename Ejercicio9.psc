Funcion resultado <- calcularedad ( nacimiento, fechaactual )
	DiaN = ConvertirANumero(SubCadena(nacimiento,0,2))
	MesN = ConvertirANumero(SubCadena(nacimiento,4,5))
	A�oN = ConvertirANumero(SubCadena(nacimiento,7,10))
	
	Dia = ConvertirANumero(SubCadena(fechaactual,0,2))
	Mes = ConvertirANumero(SubCadena(fechaactual,4,5))
	A�o = ConvertirANumero(SubCadena(fechaactual,7,10))
	
	si MesN < Mes Entonces
		resultado = A�o - A�oN
	SiNo
		si DiaN < Dia Entonces
			resultado = A�o - A�oN
		SiNo
			resultado = A�o - A�oN - 1
		FinSi
	FinSi
Fin Funcion

Algoritmo Ejercicio9
	//9.Una empresa que contrata personal requiere determinar la edad de las personas que solicitan trabajo, pero cuando se les realiza la entrevista solo se les pregunta
	//el a�o en que nacieron. Realice el diagrama de flujo y pseudocodigo que representen el algoritmo para solucionar este problema.
	
	Repetir
		Borrar Pantalla
		escribir "digite la fecha actual con formato xx/xx/xxxx"
		leer fechaactual
	Hasta Que longitud(fechaactual) = 10
	
	Repetir
		Borrar Pantalla
		escribir "ingrese la fecha en que nacio la persona en formato xx/xx/xxxx"
		leer nacimiento
	Hasta Que longitud(nacimiento) = 10
	
	Borrar Pantalla
	escribir "su edad es de " calcularedad(nacimiento,fechaactual) "a�os"
	
FinAlgoritmo
