Funcion resultado <- Sacarbase ( basex,alturax )
	resultado = basex * alturax
Fin Funcion

Algoritmo Ejercicio2
	//2. Se requiere conocer el area de un rectangulo. Realice un algoritmo para tal n y representelo mediante un diagrama de flujo y el pseudocodigo 
	//para realizar este proceso.
	//Como se sabe, para poder obtener el area del rectangulo, primeramente se tiene que concoer la base y la altura, y una vez obtenidas, se representa el resultado
	
	escribir "escriba la base (ancho) del rectangulo en metros"
	leer base
	
	escribir "escriba la altura del rectangulo en metros"
	leer altura
	
	escribir ""
	
	escribir "el area del rectangulo es de " Sacarbase(base,altura) " metros cuadrados"
FinAlgoritmo
