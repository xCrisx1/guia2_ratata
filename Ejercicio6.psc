Funcion resultado <- calcularpulgada ( metro )
	// 1 pulgada equivale a 0.0254 metros
	
	resultado = metro / 0.0254
	
Fin Funcion

Algoritmo Ejercicio6
	//6. Una modista, para realizar sus prendas de vestir, encarga las telas al extranjero. Para cada pedido, tiene que proporcionar las medidas de la tela en
	//pulgadas, pero ella generalmente las tiene en metros. Realice un algoritmo para ayudar a resolver el problema, determinando cuantas pulgadas debe pedir
	//con base en los metros que requiere. Representelo mediante el diagrama de flujo y el pseudocodigo(1 pulgada = 0.0254 m)
	
	escribir "¿Cuantos metros desea enviar en su pedido?"
	leer metros
	
	escribir ""
	escribir "En total debe enviar " calcularpulgada(metros) " pulgadas"
FinAlgoritmo
