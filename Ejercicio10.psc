Funcion resultado <- calcularpasaje ( hora,cobro )
	tiempo = ConvertirANumero(SubCadena(hora,0,1))
	
	tiempo2 = ConvertirANumero(SubCadena(hora,3,4))
	
	si tiempo2 > 0 Entonces
		tiempo2 = 1
	SiNo
		tiempo2 = 0
	FinSi
	
	vlrtiempo = tiempo + tiempo2
	
	resultado = vlrtiempo * cobro
Fin Funcion

Algoritmo Ejercicio10
	//10.Un estacionamiento requiere determinar el cobro que debe aplicar a las personas que lo utilizan. Considere que el cobro es con base en las horas que lo disponen
	//y que las fracciones de hora se toman como completas y realice un diagrama de flujo y pseudocodigo que representen el algoritmo que permina determinar el cobro
	
	Repetir
		escribir "�cuantas horas lo utiliz�? (escribir en formato xx:xx)"
		leer horas
	Hasta Que Longitud(horas) = 5
	
	escribir "�cuanto se cobra por hora?"
	leer cobros
	
	escribir "el cobro es de $" calcularpasaje(horas,cobros)
FinAlgoritmo
