Funcion resultado <- buscarPunto ( valor1 )
	cadena1 = ConvertirATexto(valor1)
	
	max = Longitud(cadena1)
	
	Para i<-1 Hasta max Con Paso 1 Hacer
		si Subcadena(cadena1,i - 1,i - 1) = "." y encontro = 0 entonces
			encontro = i
		FinSi
	Fin Para
	
	resultado = encontro
Fin Funcion

Funcion resultado <- SoloDecimales ( valorx )
	posicionpunto = buscarpunto(valorx)
	posicionmaxima = Longitud(ConvertirATexto(valorx))
	
	resultado = ConvertirANumero(Subcadena(ConvertirATexto(valorx),posicionpunto,posicionmaxima - 1))
Fin Funcion

Funcion resultado <- calcularTiempo ( distancia,velocidad )
	tiempo = distancia / velocidad
	
	posicionpunto = buscarpunto(tiempo)
	
	si posicionpunto > 0 Entonces
		horas = ConvertirANumero(SubCadena(ConvertirATexto(tiempo),0,posicionpunto - 2))
		minutos = ConvertirANumero(subcadena(ConvertirATexto(SoloDecimales(tiempo)),0,1))
		si minutos >= 60 entonces
			horas = horas + 1
			minutos = minutos - 60
			contenedor = contenedor + ConvertirATexto(horas) + ":" + ConvertirATexto(minutos)
		SiNo
			contenedor = contenedor + ConvertirATexto(horas) + ":" + ConvertirATexto(minutos)
		FinSi
		
		resultado = contenedor
	SiNo
		resultado = ConvertirATexto(tiempo) + ":00"
	FinSi
Fin Funcion

Algoritmo Ejercicio14
	//14.Se requiere determinar el tiempo que tarda una persona en llegar de una ciudad a otra en bicicleta, considerando que lleva una velocidad constante.
	//Realice un diagrama de flujo y pseudocodigo que representen el algoritmo para tal n.
	
	escribir "ingrese la distancia de su ciudad a su destino en kilometros"
	leer kilometros
	
	escribir "ingrese la velocidad en k/h"
	leer velocidad
	
	escribir ""
	
	Escribir "se demorara aproximadamente " calcularTiempo(kilometros,velocidad) " hrs"
FinAlgoritmo
